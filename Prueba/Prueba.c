/*  Author: Judit Mart�nez
										 Signal <-> MAQUETA <-> MICRO
									     en1  <->  PIN 26 <-> PK2(03) 
										 bk1  <->  PIN 24 <-> PK1(02) 
										 SW2  <->  PIN 33 <-> PK0(01) 
										 SO2  <->  PIN 21 <-> PE7(16) (Input Capture Pin  Timer 3)
										 SO3  <->  PIN 23 <-> PL1(28) (Input Capture Pin  timer 5) */ 
#include <stdio.h>
#include <avr/io.h> 
#include <math.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>
#define F_CPU 8000000;
#define PK0_MASK	0b00000001				// 0b00000001		pin change interrupt pin k0
#define BK1_ON_MASK 0x04					//  0b00000100
#define timerConf	0b01000010				//  0b01000010		Rising edge trigger, Timer = CPU Clock/8 -or- 8e6/8 -or- 1MHz
#define Open		0b00000010				//	clear break signal
#define Close		0b00000100				//	set break signal
 uint16_t TS02Rising;						// TS02 rising edge first IR Sensor
 uint16_t TS02Falling;						// TS03 falling edge
 uint16_t TS03Rising;						// TS03 rising edge Second IR Sensor
 uint16_t TS03Falling;						// TS03 falling edge
 uint16_t Tiempo_b;					// falling time - rising time s2.
 uint16_t Tiempo_a;					// rising time s1 rising time s2.
 uint32_t R;									// Ratio
 volatile int8_t SW2;						// 
 volatile uint8_t m;							// 
 int i;
 volatile uint16_t temporizador;
			

void setup(void){//  Setup
		 cli();								// Disable interrupts
		 DDRL =  0b11111101;				// PL1 as input (ICP5).
		 DDRE =  0b01111111;				// PE7 as input (ICP3).	
		 DDRK  = 0b00000110;				// PK 1 (bk1) and 2 (en1) as outputs, PK0 (SW2) as input.
		 PORTK = 0b00000110; 				// set break & Enable signals.						
		 PCICR = 0b00000010;				// external interrupts pcint 16 to 23
		 PCIFR = 0x01;						// clear flags.
		 PCMSK0= 0x01;									
		 PCMSK2= PK0_MASK;					// enable pcint16 on pin k0				
		 setupTimer3();						// set timer 3
		 setupTimer5();						// set timer 5
		 sei();								// Enable interrupts	
				}

/*void setupTimer1(){			 // setup timer1 this timer for tempo 
		TCCR1A = 0b10000000;	 // output compare clear on compare match
		TCCR1B = timerConf;		 // (01000010) Rising edge trigger, Timer = CPU Clock/8 -or- 8e6/8 -or- 1MHz
		TCCR1C = 0;				 // normal mode
		TIMSK1 = 0b00000011;	 // overflow interrupts enable and output compare
		OCR1A   = 100000;			 //	100000 microseconds 100 mili second
		TCNT1 = 0;}		    	 // start from 0*/
void setupTimer3()		{					 // setup timer3       
		TCCR3A = 0;							 // normal mode
		TCCR3B = timerConf;					 // (01000010) Rising edge trigger, Timer = CPU Clock/8 -or- 8e6/8 -or- 1MHz
		TCCR3C = 0;							 // normal mode
		TIMSK3 = 0b00100000;				 // (00100001) Input capture and overflow interrupts enabled
		TCNT3 = 0;	}						 // start from 0
void setupTimer5()	{						 // setup timer5
		TCCR5A = 0;							 // normal mode
		TCCR5B = timerConf;				     // (01000010) Rising edge trigger, Timer = CPU Clock/8 -or- 8e6/8 -or- 1MHz
		TCCR5C = 0;							 // normal mode
		TIMSK5 = 0b00100000;				 // (00100001) Input capture and overflow interrupts enabled
		TCNT5 = 0;		}					 // start from 0
/*ISR(TIMER1_COMPA_vect){		//output compare on 100000 micro seconds 	
	if (temporizador== 100)	{temporizador=0	;} 
	else	{temporizador ++;	}
	}*/



ISR(TIMER5_CAPT_vect){						// PULSE DETECTED IN ICP3!  (interrupt automatically triggered, not called by main program)
		if ((TCCR5B & 0b01000000)!=0){
			TS02Rising= ICR5;				// capture the timer value in the rising edge
			TCCR5B = 0b00000010;}			// configure the input capture on falling edge
		else{
			TS02Falling= ICR5;				// capture the timer value in falling edge
			TCCR5B = 0b01000010;			// reconfigure the input capture on rising edge
			TCNT5=0;
			Tiempo_a= TS03Rising-  TS02Rising;
			Tiempo_b= TS03Falling- TS03Rising;
		TS02Falling=0;	
		
			}
					}						// Saves time when coin interrupts SO2.

ISR(TIMER3_CAPT_vect){						// PULSE DETECTED IN ICP5!  (interrupt automatically triggered, not called by main program)
		if ((TCCR3B & 0b01000000)!=0)	{
			TS03Rising = ICR3;				// Saves time when coin interrupts SO2.
			TCCR3B = 0b00000010;}			// 
		else	{
			TS03Falling = ICR3;				// Save time when SO2 stops detecting coin.
			TCCR3B = 0b01000010;			// 		
			TCNT3=0;
				}}
			
//Podemos asociar un Output compare tras mirar el tiempo del �ltimo sensor (cuando damos valor a SO3Falling) a X (tiempo a determinar �3*Tb?) 
ISR( PCINT1_vect ){ 
		SW2 = PINK & PK0_MASK;
		if(SW2==0)	{		
			PORTK |= BK1_ON_MASK;		}	//set the break if pk0==0.	
		else		{		
			PORTK &= ~BK1_ON_MASK;		}	//PK0 = 1, clear the break signal.
     				}			



int main(void){
		setup();
		volatile uint32_t Ratio;						
		volatile uint8_t euro=0;
		volatile uint16_t saldo=0;
		while(1)	{		
		/*t1=TS02Rising;
		t2=TS02Falling;
		t3=TS03Rising;
		t4=TS03Falling;
		ta=t1-t2;
		tb=t1-t3;
		euro=m;
		PORTK=Open;
		PORTK=Close;
		PORTK=Open;
		PORTK=Close;
		PORTK=Open;
		PORTK=Close;
		PORTK=Open;
		PORTK=Close;
		*/		
		
		
		/*if (TS03Falling!=0){
			Tiempo_b=TS02Rising-TS03Rising;
		}
		else{
			Tiempo_b=0;
		}
		if (TS02Falling!=0){
			Tiempo_a= TS03Rising-TS03Falling;
			}
		else{
			Tiempo_a=0;
			}		
			*/
			//R = Tiempo_b/Tiempo_a;
			/*if ((Tiempo_b>40751)&&(Tiempo_b<42067))
			{
				PORTK=Open;
			}
			else
			{
				PORTK=Close;
				PORTK=Open;
			}*/
				
		if ((Tiempo_b!=0) && (Tiempo_a!=0)){
			Ratio=((Tiempo_a/Tiempo_b)*1000000);	
			Tiempo_a=0;
			Tiempo_b=0;		
						}		
		else{	Ratio=0;}
									
		/*if((Ratio>1305882353) && (Ratio<1317073171)){
				euro=100;
				saldo+=100;
				PORTL &= ~BK1_ON_MASK;	}	//PB0 a 1, desactiva el freno.
		if((Ratio>1419753086) && (Ratio<1407407407)){
				euro=50;
				saldo+=50;}
		if((Ratio>1252873563) && (Ratio<1271604938)){
				euro=20;
				saldo+=20;}
		if((Ratio>1121951220) && (Ratio<1125000000)){
				euro=10;
				saldo+=10;}*/
				
	/*TS02Rising = ICR3;      // Save time when SO2 activates (detects coin).
	 TS03Rising = ICR5;      // Save time when SO3 activates.
	 if (TS02Rising>0 & TS02falling>0)
	 {	Tiempo_b=TS02falling-TS02Rising;}
		PORTK &= ~BK1_ON_MASK ; //0bXXXXXXX01X; // Freno desact.Enable act.	 
		for (volatile unsigned int i=0; i<200; i++) {		}; //Delay,no s� de cu�ntos us,pero sirve para un ciclo de apertura+cierre de la barrera.
		PORTK |= BK1_ON_MASK; //0bXXXXX11X;	//Freno act.Enable act 
		for (volatile unsigned int i=0; i<650; i++) {		};*/
	}
}


